import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class PingPongRightTest {

	private ByteArrayOutputStream baos;
	private PrintStream ps;		
	private PrintStream psOrig;
						
	@Before
	public void setUp() throws Exception {
		//store current stdout PrintStream
		psOrig = System.out;
		//set stdout PrintStream to a ByteArrayOutputStream
		baos = new ByteArrayOutputStream();
		ps = new PrintStream(baos);		
		System.setOut(ps);
		
		
	}

	@After
	public void tearDown() throws Exception {
		//restore stdout PrintStream
		System.setOut(psOrig);
		//clean up output streams
		if (baos != null) {
			baos.close();
			baos = null;
		}
		if (ps != null) {
			ps.close();
			ps = null;
		}
	}

	
	/**
	 * Tests {@link PingPongRight#main(String[])} expected output to
	 * stdout.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testPingPongRight() throws Exception {
		String expected = createExpectedPingPongString();
		
		
		PingPongRight.main(null);
		String actual = baos.toString();
		
		assertEquals(expected, actual);
	}

	
	/**
	 * Tests the ability to capture stdout in a String.
	 * @throws Exception
	 */
	@Test
	public void testStdOutCapture() throws Exception {
		
		//print out something
		System.out.println("foo");
		System.out.println("bar");
		//capture stdout in string
		String stdout = baos.toString();
		
		//assert that string is as expected
		assertEquals("foo" + System.getProperty("line.separator") + "bar" + System.getProperty("line.separator"), stdout);
		
	}

	
	/**
	 * Test the creation of the expected Ping-Ping output with 
	 * that obtained from a file (using Apache Commons IO library),
	 * @throws Exception
	 */
	@Test
	public void testCreateExpectedPingPongString() throws Exception {

		String dynamicPingPongStdoutString = createExpectedPingPongString();
		String fileStoredPingPongStdoutString = getExpectedPintPongStringFromFile();
		
		System.out.println("Dynamic string: ");
		System.out.println(fileStoredPingPongStdoutString);
		System.out.println("File based string: ");
		System.out.println(dynamicPingPongStdoutString);
		assertEquals(fileStoredPingPongStdoutString, dynamicPingPongStdoutString);
		
		
	}
	
	
	private String createExpectedPingPongString() {
		StringBuilder sb = new StringBuilder();
        sb.append("Ready...Set...Go!");
		sb.append(System.getProperty("line.separator"));
		int count = 20;
		for (int i = 1, j = 1; i <= count; i++) {
			if (i % 2 == 0) {
				sb.append("Ping!(" + j + ")");
				sb.append(System.getProperty("line.separator"));
				sb.append("Pong!(" + j + ")");
				sb.append(System.getProperty("line.separator"));
				j++;
			}
		}
		sb.append("Done!");
		sb.append(System.getProperty("line.separator"));
		return sb.toString();
	}



	private String getExpectedPintPongStringFromFile() throws IOException {
		StringBuilder sb = new StringBuilder();
		File file = new File("test/expected_stdout.txt");
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
		
			while(fis.available() > 0) {
				sb.append((char)fis.read());
			}
		} finally {
			if (fis != null) {
				fis.close();
				fis = null;
			}
		}
		
		return sb.toString();
	}
}

