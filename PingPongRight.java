// Import the necessary Java synchronization and scheduling classes.
import java.util.concurrent.Semaphore;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;

/**
 * @class PingPongRight
 *
 * @brief This class implements a Java program that creates two
 *        instances of the PlayPingPongThread and start these thread
 *        instances to correctly alternate printing "Ping" and "Pong",
 *        respectively, on the console display.
 */
public class PingPongRight
{
    /**
     * @class SimpleSemaphore
     *
     * @brief This class provides a simple counting semaphore
     *        implementation using Java a ReentrantLock and a
     *        ConditionObject.
     */
    static public class SimpleSemaphore
    {
        /**
         * Constructor initialize the data members. 
         */
        public SimpleSemaphore (int maxPermits)
        { 
            // TODO - You fill in here.
            this.bound = maxPermits;
        }

        /**
         * Acquire one permit from the semaphore.
         */
        public void acquire() throws InterruptedException
        {
            // TODO - You fill in here.
            lock.lock();
            //lock.lockInterruptibly();
            try {
                while(this.signals == bound) condition.await();
                this.signals++;
                //System.out.format("acquire(): %d%n", signals);
                condition.signal();
            } finally {
                lock.unlock();
            }
        }

        /**
         * Return one permit to the semaphore.
         */
        void release() throws InterruptedException
        {
            // TODO - You fill in here.
            lock.lock();
            //lock.lockInterruptibly();
            try {
                while(this.signals == 0) condition.await();
                this.signals--;
                //System.out.format("release(): %d%n", signals);
                condition.signal();
            } finally {
                lock.unlock();
            }
        }

        /**
         * Use a ReentrantLock to protect the critical section.
         */
        // TODO - You fill in here.
        private static final Lock lock = new ReentrantLock(true);

        /**
         * Use a ConditionObject to wait while the number of permits
         * is 0.
         */
        // TODO - You fill in here.
        private static final Condition condition = lock.newCondition();

        /**
         * A count of the number of available permits.
         */
        // TODO - You fill in here.
        private int signals = 0;
        private int bound   = 0;

    }

    /**
     * Number of iterations to run the test program.
     */
    public static int mMaxIterations = 10;
    
    /**
     * Latch that will be decremented each time a thread exits.
     */
    //public static CountDownLatch latch = null; // TODO - You fill in here
    public static CountDownLatch latch = new CountDownLatch(2);

    /**
     * @class PlayPingPongThread
     *
     * @brief This class implements the ping/pong processing algorithm
     *         using the SimpleSemaphore to alternate printing "ping"
     *         and "pong" to the console display.
     */
    public static class PlayPingPongThread extends Thread
    {
        /**
         * Constructor initializes the data member.
         */
        //public PlayPingPongThread (/* TODO - You fill in here */)
        public PlayPingPongThread (String name, CountDownLatch barrier)
        {
            // TODO - You fill in here.
            this.name = name;
            this.barrier = barrier;
        }

        /**
         * Main event loop that runs in a separate thread of control
         * and performs the ping/pong algorithm using the
         * SimpleSemaphores.
         */
        public void run () 
        {
            // TODO - You fill in here.
            //System.out.format("%s thread running..%n", name);
            for (int i = 1; i <= mMaxIterations; ++i) {
                //System.out.format("%s %d%n", name, i);
                if (name == "Ping") {
                    try {
                        ping.acquire();
                        System.out.format("%s!(%d)%n", name, i);
                        ping.release();
                    } catch (InterruptedException ex) {
                      ex.printStackTrace();
                    } //finally { pong.release(); }
                }
                if (name == "Pong") {
                    try {
                        pong.acquire();
                        System.out.format("Pong!(%d)%n", i);
                        pong.release();
                    } catch (InterruptedException ex) {
                      ex.printStackTrace();
                    } //finally { ping.release(); }
                }
                //barrier.countDown();
            }
            barrier.countDown();
        }

        /**
         * Number of iterations to run.
         */
        // TODO - You fill in here.
        private static int game = 0;

        /**
         * Number of iterations to run.
         */
        // TODO - You fill in here.
        String name;
        CountDownLatch barrier;

        /**
         * The two SimpleSemaphores use to alternate pings and pongs.
         */
        // TODO - You fill in here.
        private SimpleSemaphore ping = new SimpleSemaphore(1);
        private SimpleSemaphore pong = new SimpleSemaphore(1);
    }

    /**
     * The main() entry point method into PingPongRight program. 
     */
    public static void main(String[] args) {
        //try {         
            // Create the ping and pong SimpleSemaphores that control
            // alternation between threads.

            // TODO - You fill in here.

            System.out.println("Ready...Set...Go!");

            // Create the ping and pong threads, passing in the string
            // to print and the appropriate SimpleSemaphores.
            PlayPingPongThread ping =
                //new PlayPingPongThread(/* TODO - You fill in here */);
                new PlayPingPongThread("Ping", latch);
            PlayPingPongThread pong =
                //new PlayPingPongThread(/* TODO - You fill in here */);
                new PlayPingPongThread("Pong", latch);
            
            // Initiate the ping and pong threads, which will call the
            // run() hook method.
            ping.start();
            pong.start();

            // Use barrier synchronization to wait for both threads to
            // finish.
            //CountDownLatch barrier = new CountDownLatch(2);

            // TODO - replace this line:
            try {
                latch.await(); 

            } catch (InterruptedException ex) {
                System.out.println(ex);
            }

        //    throw new java.lang.InterruptedException();
        //} 
        //catch (java.lang.InterruptedException e)
        //    {}

        System.out.println("Done!");
    }
}
